# ics-ans-role-e3-compile

Ansible role to e3-compile workstations.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-e3-compile
```

## License

BSD 2-clause
